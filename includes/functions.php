<?php
/**
 * Displays an error Message
 * 
 * @param string $msg the message to display 
 */
function errorMessage($msg)
{
	if (!isset($_SESSION['error']) || !is_array($_SESSION['error']))
	{
		$_SESSION['error']=array($msg);
	}
	else
	{
		$_SESSION['error'][]=$msg;
	}
}

/**
 * mysql_query wrapper
 * 
 * @param string $query SQL query 
 */
function dbQuery($query,$conn)
{
	$result=mysql_query($query,$conn);
	
	//check result
	if (!$result) {
		$message  = 'Invalid query: ' . mysql_error() . "\n";
		$message .= 'Whole query: ' . $query;
		die("You can't hack this site");
	}
	
	return $result;
}