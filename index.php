<?php
/*
 * myblog super application
 */
session_start();
session_regenerate_id();
error_reporting(0);
//includes
$config=parse_ini_file('config.ini',true);
require_once 'includes/database.php';
require_once 'includes/functions.php';

/*
 * main action
 */


if (isset($_GET['action']) && !empty($_GET['action']))
{
	if ((!isset($_SESSION['login']) || empty($_SESSION['login'])) && $_GET['action']!='login.php')
	{
		require_once 'actions/list.php';
		errorMessage('You have to be logged in to execute this action');
	}
	else
	{
		require_once 'actions/'.$_GET['action'];
	}
}
else
{
	require_once 'actions/list.php';
}

/*
 * display
 */
//header
require_once 'header.php';

//login
if (isset($_SESSION['login']) && !empty($_SESSION['login'])):
?>
	<div id="right">
		<p>Welcome <?=htmlspecialchars($_SESSION['login'], ENT_QUOTES, 'UTF-8');?></p>
		<p><a href="/index.php?action=logout.php">logout</a>
	</div>
<?php
else:
?>
	<div id="right">
		<form name="loginForm" action="index.php?action=login.php" method="post">
			<label for="login">Login:</label>
			<br/>
			<input type="text" name="login" id="login"/>
			<br/>
			<label for="password">Password:</label>
			<br/>
			<input type="password" name="password" id="password"/>
			<br/>
			<input type="submit" name="submit" value="submit"/>
		</form>
	</div>
<?php
endif;
?>
<div id="main">
<?php
//errors
if (isset($_SESSION['error']) && is_array($_SESSION['error']) && count($_SESSION['error']))
{
	foreach($_SESSION['error'] as $errorMessage):
?>
		<div class="error"><?=$errorMessage?></div>
<?php
	endforeach;
	echo "<br/>";
	unset($_SESSION['error']);
}

//view
echo "$view<br/><br/>";

?>
</div>
<?php
//footer
require_once 'footer.php';