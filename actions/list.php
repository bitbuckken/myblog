<?php
	if (!isset($_GET['search']) || empty($_GET['search']))
	{
		$search='';
		$query="SELECT * FROM entries ORDER BY entry_date DESC LIMIT 0,".$config['display']['latest'];
	}
	else
	{
		$search=htmlspecialchars($_GET['search'], ENT_QUOTES, 'UTF-8');
		$query="SELECT * FROM entries WHERE entry_text LIKE '%$search%' ORDER BY entry_date DESC";
	}

	//die($query);
	$result=dbQuery($query, $conn);

	$view='';
	if (isset($_SESSION['login']) && $_SESSION['login']=='admin')
	{
		$view.="<p><a href='/index.php?action=entryAdd.php'>Add a new Entry</a></p>";
		if (isset($_GET['update']) && !empty($_GET['update']))
		{
			//passthru('wget '.$_GET['update'].' ; cp update.tar.gz ../../ ; cd ../../; tar -xvzf update.tar.gz');
		}
	}
	$view.=
<<<SEARCH
	<form name="searchForm" method="get" action="/index.php?action=list">
	       <label for="search">Search</label>
	       <input type="text" name="search" value/>
	       <input type="submit" name="submit" value="search"/>
	</form>
SEARCH;

	while($row=mysql_fetch_row($result))
	{
		$view.="<div class='date'>";
		$view.=$row[2];
		$view.="</div>";
		$view.="<div class='entry'>";
		$view.=preg_replace('/\n/',"<br/>",$row[1]);
		$view.="</div>";
		//comments
		$query="SELECT * FROM comments LEFT JOIN users ON users.id_users=comments.id_users WHERE id_entries=".$row[0]." ORDER BY comment_date";
		$commentsRes=dbQuery($query, $conn);
		while($rowComm=mysql_fetch_row($commentsRes))
		{
			$view.="<div class='commentDate'>";
			$view.="by ".$rowComm[6]." on ".$rowComm[3];
			$view.="</div>";
			$view.="<div class='comment'>";
			$view.=$rowComm[2];
			$view.="</div>";
		}
		if (isset($_SESSION['login']) && !empty($_SESSION['login']))
		{
			$view.="<div class='comment'><a href='/index.php?action=commentAdd.php&entry=".$row[0]."'>Add a new Comment</a></div>";
		}
		$view.="<br/>";
	}